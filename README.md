# PCB design of the CLICpix2 and C3PD chipboards.

## Tool: Altium Designer

## Documentation:
* [CLICpix2 manual](https://www.overleaf.com/read/bmcvcbvckksc)
* [C3PD manual](https://gitlab.cern.ch/Caribou/Caribou-HW-CLICpix2_C3PD/raw/master/doc/c3pd_manual.pdf)

![picture alt](https://gitlab.cern.ch/Caribou/Caribou-HW-CLICpix2_C3PD/raw/master/doc/chipboards.jpg)